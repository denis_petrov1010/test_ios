/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        console.log("device ready");

        this.loadAppPage();
    },
    loadAppPage: function() {
        error = false;
        appPage = cordova.InAppBrowser.open('https://www.arcgis.com/apps/opsdashboard/index.html#/85320e2ea5424dfaaa75ae62e5c06e61',  '_blank', 'hidden=yes,location=no,zoom=no');
        
        appPage.addEventListener('loaderror', function () {
            error = true;
            var errorSelector = document.getElementById("error");
            errorSelector.setAttribute('style', 'display:block;');
            appPage.close();
        });
        appPage.addEventListener('loadstop', function() {
            console.log("loadstop");
            appPage.show();
            //working
            // appPage.insertCSS({ code: "#ember9{display: none !important;}" });
            // appPage.insertCSS({ code: "#ember10{display: none !important;}" });
            appPage.insertCSS({ code: "#ember12{display: none !important;}" });
            // appPage.insertCSS({ code: "#ember71{display: none !important;}" });
            appPage.insertCSS({ code: "#ember90{display: none !important;}" });
            appPage.insertCSS({ code: "#ember118{display: none !important;}" });
            appPage.insertCSS({ code: "#ember120{display: none !important;}" });
            appPage.insertCSS({ code: "#ember136{display: none !important;}" });
            //not working
            appPage.insertCSS({ file: "css/inject.css" });
        });
        appPage.addEventListener('loaderror', function() {
            console.log("loaderror");
            appPage.show(false);
        });
        appPage.addEventListener('exit', function() {
            console.log("exit");
            if (!error) {
                navigator.app.exitApp();
            }
        });
        appPage.addEventListener('loadstart', function() {
            console.log("loadstart");
            //not works
            appPage.executeScript({file: "js/inject.js"});
            // works
            appPage.executeScript({code: "(function() { console.log('I`m in!'); })()"});
        });
    }
};

app.initialize();